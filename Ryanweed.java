public class Ryanweed {

  private double hp;
  private int spleens;
  // hand raise how does a weed have spleens?
  public Ryanweed(double hp, int spleens) {
    this.hp = hp;
    this.spleens = spleens;
  }

  public double getHP() {
    return hp;
  }

  public void setHP(double hp) {
    this.hp = hp;
  }

  public int getSpleens() {
    return spleens;
  }

  public void setSpleens() {
    this.spleens = spleens;
  }

}