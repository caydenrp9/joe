public class HongFish {
  
  private double hongHP;
  private double clownPower;

  public HongFish(double hongHP, double clownPower) {
    this.hongHP = hongHP;
    this.clownPower = clownPower; 
    
  }

  public double getHongHP(){
    return hongHP;
  }

  public double getClownPower(){
    return clownPower;
  }

  public void setHongHP(double hongHP){
    this.hongHP = hongHP;
  }

  public void setClownPower(double clownPower){
    this.clownPower = clownPower;
  }
}