public class Clownfish {
  
  private double clownHealth;
  private double clownLifespan;
  private double clownHunger;
  
  public Clownfish (double clownLifespan, double clownHealth, double clownHunger){

    this.clownHealth = clownHealth;
    this.clownLifespan = clownLifespan;
    this.clownHunger = clownHunger;

  }
  public double getclownHealth(){

    return clownHealth;

  }
  public double getclownLifespan(){

    return clownLifespan;

  }
  public double getclownHunger(){

    return clownHunger;

  }
  public void setclownHealth(double clownHealth){
    
    this.clownHealth = clownHealth;
  }
  public void setclownHunger(double clownHunger){
    
    this.clownHunger = clownHunger;

  }
}