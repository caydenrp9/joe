public class Emu {
  private double destructiveCapability;
  private double naturalLifespan;
  private double durability;
  private double plumage;
  
  public Emu(double destruction, double life, double hp, double plumage) {
    this.destructiveCapability = destruction;
    this.naturalLifespan = life * Double.POSITIVE_INFINITY;
    this.durability = hp * Double.POSITIVE_INFINITY;
    this.plumage = plumage;
  }

  public double getDestructiveCapability() {
    return destructiveCapability;
  }

  public void setDestructiveCapability(double destructiveCapability) {
    this.destructiveCapability = destructiveCapability;
  }

  public double getNaturalLifespan() {
    return naturalLifespan;
  }

  public double getDurability() {
    return durability;
  }

  public double getPlumage() {
    return plumage;
  }

}