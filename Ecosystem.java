import java.util.Random;

public class Ecosystem {
 
  private int numClownfish;
  private int numHongfish;
  private int numRyanweed;
  
  private Clownfish[] clownArray;
  private Ryanweed[] bruhArray;
  private Hongfish[] honkArray;

  private Random random;
  
  /**
   * Creates a new ecosystem with fish, ryanweed, and chance of drought.
   * @param numClownFish Number of Clownfish
   * @param numHongFish Number of Hongfish
   * @param numRyanWeed Number of Ryanweed
   * @param drought - whether or not there is drought
   */
  public Ecosystem(int numClownFish, int numHongFish, int numRyanWeed, boolean drought) {  
    clownArray = new Clownfish[numClownFish]; 
    bruhArray = new Ryanweed[numHongFish];
    honkArray = new Hongfish[numRyanWeed];
    this.random = new Random();

    for (int i = 0; i < clownArray.length; i++) {
      clownArray[i] = new Clownfish(random.nextInt(69), random.nextInt(420), random.nextInt(9001));
      //https://docs.oracle.com/javase/8/docs/api/java/util/Random.html#nextInt-int-
    }
    
    for (int i = 0; i < bruhArray.length; i++) {
      bruhArray[i] = new Ryanweed(random.nextInt(69),random.nextInt(420), random.nextInt(9001));
    }
    
    for (int i = 0; i < honkArray.length; i++) {
      honkArray[i] = new Hongfish(random.nextInt(69), random.nextInt(420), random.nextInt(9001));
      
    }
  }

}